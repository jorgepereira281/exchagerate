package challenge.ExchangeRate.api;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import challenge.ExchangeRate.requests.HttpRequests;
import reactor.core.publisher.Mono;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/exchange")
public class ExchangeRateApi {

    @Autowired
    HttpRequests requests;

    @GetMapping("/rate")
    public Mono<Map> getExchangeRate(@RequestParam("currency_a") String currencyA,
                                     @RequestParam("currency_b") String currencyB) {
        return requests.getExchangeRate(currencyA, currencyB);
    }

    @GetMapping("/rates")
    public Mono<Map> getAllExchangeRates(@RequestParam("currency_a") String currencyA) {        
        return requests.getAllExchangeRates(currencyA);
    }

    @GetMapping("/convert-value")
    public Mono<Map> convertCurrencyValue(@RequestParam("currency_a") String currencyA,
                                          @RequestParam("currency_b") String currencyB,
                                          @RequestParam("value") Double value) {
        return requests.getCurrencyValueConversion(currencyA, currencyB, value);
    }

    @GetMapping("/convert-many")
    public Mono<Map> convertCurrencyValueToMany(@RequestParam("currency_a") String currencyA,
                                                @RequestParam("currency_list") String currencyList,
                                                @RequestParam("value") Double value) {
        return requests.getCurrencyValueConversion(currencyA, currencyList, value);
    }

}