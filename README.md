# Exchange Rate API

Rest API to provide exchange rates and currency conversions.
This project was made using Spring Boot Java Framework.

### Requirements
To run this project it is required to have installed:
* [Java 8 JDK](https://www.oracle.com/technetwork/java/javase/overview/index.html)
* [Maven](https://maven.apache.org/)

### Project Structure
- [HttpRequests.java](./src/main/java/challenge/ExchangeRate/requests/HttpRequests.java) - class used to make HTTP requests to the external service, and also manipulates data for the currency value conversion.
- [ExchangeRateApi.java](./src/main/java/challenge/ExchangeRate/api/ExchangeRateApi.java) - controller to provide a REST API.
- [ExchangeRateApplication.java](./src/main/java/challenge/ExchangeRate/ExchangeRateApplication.java) - main class of the project.
- [application.properties](./src/main/resources/application.properties) - configuration file for endpoints and cache parameters.

### How to run it
First the API needs to be running to accept HTTP requests. To run the Spring Boot project, run the following command in the root of the project:
```
mvn spring-boot:run
```
This will download the dependencies from maven central, compile and start a HTTP server on port `8080`.

### Test it
To test the REST API, a swagger web interface is available with all the API methods of the Exchange Rate application. 
To use it, open with a browser the `index.html` file under the swagger folder, and execute the available requests.






