package challenge.ExchangeRate.requests;

import java.util.Map;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
@CacheConfig(cacheNames = {"rate", "rates", "conversion"})
public class HttpRequests{
    
    @Value("${exchangeratesapi.host}")
    private String exchangeRatesApiHost;
    
    private WebClient client;

    @PostConstruct
    private void init(){
        client = WebClient.create(exchangeRatesApiHost);
    }

    @Cacheable("rate")
    public Mono<Map> getExchangeRate(String currencyA, String currencyB){
        return client.get().uri(builder -> builder.path("latest")
                                                  .queryParam("base", currencyA)
                                                  .queryParam("symbols", currencyB)
                                                  .build())
                                    .accept(MediaType.APPLICATION_JSON)                                    
                                    .exchange()
                                    .flatMap(response -> response.bodyToMono(Map.class));
    }

    @Cacheable("rates")
    public Mono<Map> getAllExchangeRates(String currencyA){        
        return client.get().uri(builder -> builder.path("latest")
                                                  .queryParam("base", currencyA)
                                                  .build())
                                    .accept(MediaType.APPLICATION_JSON)
                                    .exchange()
                                    .flatMap(response -> response.bodyToMono(Map.class));        
    }

    @Cacheable("conversion")
    public Mono<Map> getCurrencyValueConversion(String currencyA, String currencyB, double value){
        Mono<Map> exchangeRate = getExchangeRate(currencyA, currencyB);
        return exchangeRate.flatMap(response -> {
            //Replaces the exchange rate with the converted value
            if (response.containsKey("rates")){
                Map<String, Double> rates = (Map) response.remove("rates");
                rates.replaceAll((currency,rate) -> rate*value);
                response.put("value", rates);
            }            
            return Mono.just(response);
        });
    }   
}