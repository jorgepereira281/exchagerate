package challenge.ExchangeRate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;


@SpringBootApplication
@EnableCaching
public class ExchangeRateApplication {
	public static void main(String[] args) throws InterruptedException {
		SpringApplication.run(ExchangeRateApplication.class, args);		
	}
}
